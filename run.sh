#!/usr/bin/env bash
set -x

docker build --platform linux/amd64 -t buildbox-e2e .
docker run -v /var/run/docker.sock:/var/run/docker.sock buildbox-e2e
