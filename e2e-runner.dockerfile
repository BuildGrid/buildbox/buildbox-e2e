# This acts as a text fixture.
# It starts BuildGrid, buildbox-casd and buildbox-worker, and then execs the
# commands given as argument.

# The fixture image contains the versions of BuildGrid and buildbox binaries
# to put under test.
FROM e2e-environment:latest

COPY bin/tests /tests

ENV BUILDGRID_SERVER="localhost:50051"
ENV CASD_ENDPOINT="localhost:50011"

# The entrypoint script starts everything before it invokes the given argument.
# This allows to create and run scripts that execute a single test instance,
# which is guaranteed to run inside a clean and isolated environment.
COPY bin/create-test-environment.sh /buildbox-e2e/bin
ENTRYPOINT ["create-test-environment.sh"]
