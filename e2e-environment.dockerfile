# This image allows rebuilding specific versions of the various projects
# being tested, replacing the versions in the base image build from the
# HEADs of the repositories.
#
# Custom branches/revisions of buildbox projects to test can be placed under
# src/.

ARG BASE_IMAGE=registry.gitlab.com/buildgrid/buildbox/buildbox-e2e/buildbox-e2e-base:latest
FROM ${BASE_IMAGE}

ENV BUILDBOX_E2E_DATA_DIR=/buildbox-e2e/data
ENV BUILDBOX_E2E_ROOT=/buildbox-e2e-build

# Place the optional contents of src/ in the build directory of the e2e test.
# This allows to add subdirectories with sources of buildbox projects
# to be used instead of version in the base image, built from the projects'
# main branches.
COPY src/ /buildbox-e2e-build

# Adding the location of scripts and built binaries to $PATH to make them
# simpler to find from test-case scripts:
ENV PATH="/buildbox-e2e/bin:${BUILDBOX_E2E_ROOT}/bin:${PATH}"

# Build any projects mounted into the image using `src/`
COPY bin/env-vars.sh /buildbox-e2e/bin/env-vars.sh
COPY bin/build-sources.sh /buildbox-e2e/bin/build-sources.sh
RUN build-sources.sh
