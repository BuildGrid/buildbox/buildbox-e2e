# This fetches the sources of all buildbox components, builds them, and adds
# their binaries to $PATH.
#
# The resulting image is the base used by the e2e-environment image, which
# is used to spin up a test environment for each test case.

FROM debian:bookworm

RUN apt-get update && apt-get install -y --no-install-recommends \
    bubblewrap \
    build-essential \
    clang-tools \
    cmake \
    curl \
    docker.io \
    fuse3 \
    g++ \
    git \
    jq \
    libfuse3-dev \
    libgrpc++-dev \
    libprotobuf-dev \
    libssl-dev \
    libtomlplusplus-dev \
    net-tools \
    ninja-build \
    nlohmann-json3-dev \
    pipx \
    pkg-config \
    procps \
    protobuf-compiler-grpc \
    psmisc \
    python3-minimal \
    python3-pip \
    python3-venv \
    redis \
    sudo \
    uuid-dev \
    ca-certificates \
    && apt-get clean

ARG BUILDBOX_COMMIT=HEAD
ARG BUILDGRID_COMMIT=HEAD
ARG LOGSTREAM_COMMIT=HEAD
ARG CHROOTBUILDER_COMMIT=HEAD
ARG USERCHROOT_COMMIT=HEAD

ENV BUILDBOX_E2E_BASE_ROOT=/buildbox-e2e-base-build
ENV BUILDBOX_E2E_ROOT=/buildbox-e2e-base-build

ENV BUILDBOX_E2E_DATA_DIR=/buildbox-e2e/data
ARG BUILDBOX_E2E_BIN=/buildbox-e2e/bin
ENV BUILDBOX_E2E_BIN=${BUILDBOX_E2E_BIN}

# Adding the location of scripts and built binaries to $PATH to make them
# simpler to find from test-case scripts:
ENV PATH="/root/.local/bin:${BUILDBOX_E2E_BIN}:${BUILDBOX_E2E_ROOT}/bin:${PATH}"

# 1) Fetch sources:
COPY bin/fetch-sources.sh ${BUILDBOX_E2E_BIN}/fetch-sources.sh
COPY bin/env-vars.sh ${BUILDBOX_E2E_BIN}/env-vars.sh

RUN BUILDBOX_COMMIT="$BUILDBOX_COMMIT" \
    BUILDGRID_COMMIT="$BUILDGRID_COMMIT" \
    LOGSTREAM_COMMIT="$LOGSTREAM_COMMIT" \
    CHROOTBUILDER_COMMIT="$CHROOTBUILDER_COMMIT" \
    USERCHROOT_COMMIT="$USERCHROOT_COMMIT" \
    fetch-sources.sh

# 2) Build:

COPY bin/build-sources.sh ${BUILDBOX_E2E_BIN}/build-sources.sh
RUN build-sources.sh

# data/ contains configs and sample files to build in tests:
COPY data/ ${BUILDBOX_E2E_DATA_DIR}
