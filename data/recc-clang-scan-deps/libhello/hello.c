#include "hello.h"
#include <stdio.h>

void doHello(const char *name) {
  printf("Hello %s!\n", name);
}
