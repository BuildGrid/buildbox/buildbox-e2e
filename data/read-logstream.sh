#!/bin/bash

# Usage: `read-logstream.sh path_to_bgd_binary \
#         stdout_resource_name stdout_dest_file \
#         stderr_resource_name stderr_dest_file`
#
# It will strip the write token from the resource name, spawn `bgd logstream
# read` on the result with its output redirected to the file specified.

BUILDGRID_BINARY=$1

STDOUT_WRITE_RESOURCE_NAME=$2
STDOUT_READ_RESOURCE_NAME=$(dirname "$STDOUT_WRITE_RESOURCE_NAME")
STDOUT_DESTINATION_FILE=$3


STDERR_WRITE_RESOURCE_NAME=$4
STDERR_READ_RESOURCE_NAME=$(dirname "$STDERR_WRITE_RESOURCE_NAME")
STDERR_DESTINATION_FILE=$5

echo "Launching '$BUILDGRID_BINARY logstream read $STDOUT_READ_RESOURCE_NAME'"
echo "Output redirected to '$STDOUT_DESTINATION_FILE'"
($BUILDGRID_BINARY logstream --remote="http://localhost:50055" read "${STDOUT_READ_RESOURCE_NAME}" || exit 1) | tee $STDOUT_DESTINATION_FILE &
echo "Launching '$BUILDGRID_BINARY logstream read $STDERR_READ_RESOURCE_NAME'"
echo "Output redirected to '$STDERR_DESTINATION_FILE'"
($BUILDGRID_BINARY logstream --remote="http://localhost:50055" read "${STDERR_READ_RESOURCE_NAME}" || exit 1) | tee $STDERR_DESTINATION_FILE &
