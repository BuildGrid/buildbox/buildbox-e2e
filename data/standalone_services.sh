#!/usr/bin/env bash

#
# Example of a 'distributed' buildgrid, with each service running in their
# own server
#
# - Execution:    50051
# - Bots:         50052
# - CAS:          50053
# - ActionCache:  50054
#
# Environment variables required in create-test-environment.sh:
#
# - BUILDGRID_CAS_SERVER
# - BUILDGRID_LOGSTREAM_SERVER
# - BUILDGRID_BOTS_SERVER
#

check_server_started() {
    local server=$1
    local timeout=20
    local interval=2
    local attempts=$((timeout / interval))

    for ((i = 1; i <= attempts; i++)); do
        sleep $interval
        netstat -pln | grep "$server"
        if [ $? -eq 0 ]; then
            echo "Server $server started successfully."
            return 0
        fi
    done

    echo "Server $server failed to start within $timeout seconds."
    return 1
}

# Buildgrid server that has an Execution service
bgd server start "$BUILDBOX_E2E_DATA_DIR/bgdconfig_execution.yml" &

check_server_started "50051"

# Buildgrid server that has a Bots service
bgd server start "$BUILDBOX_E2E_DATA_DIR/bgdconfig_bots.yml" &
BUILDGRID_BOTS_SERVER="localhost:50052"

check_server_started "50052"

# Buildgrid server that has a CAS (+ Bytestream + Logstream) service
bgd server start "$BUILDBOX_E2E_DATA_DIR/bgdconfig_cas.yml" &
BUILDGRID_CAS_SERVER="localhost:50053"
BUILDGRID_LOGSTREAM_SERVER="localhost:50053"

check_server_started "50053"

# Buildgrid server that has an ActionCache service
bgd server start "$BUILDBOX_E2E_DATA_DIR/bgdconfig_actioncache.yml" &

check_server_started "50054"




