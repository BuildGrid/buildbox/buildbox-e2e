#!/usr/bin/env bash

#
# Example of a 'distributed' buildgrid, with all services on one server, apart
# from 'Bots', which is a standalone service on a separate server
#
# Environment variables required in create-test-environment.sh:
# - BUILDGRID_CAS_SERVER
# - BUILDGRID_LOGSTREAM_SERVER
# - BUILDGRID_BOTS_SERVER
#

check_server_started() {
    local server=$1
    local timeout=20
    local interval=2
    local attempts=$((timeout / interval))

    for ((i = 1; i <= attempts; i++)); do
        sleep $interval
        netstat -pln | grep "$server"
        if [ $? -eq 0 ]; then
            echo "Server $server started successfully."
            return 0
        fi
    done

    echo "Server $server failed to start within $timeout seconds."
    return 1
}

# Buildgrid server that expects a remote Bots service
bgd server start "$BUILDBOX_E2E_DATA_DIR/bgdconfig_no_bots.yml" &
BUILDGRID_CAS_SERVER="localhost:50051"
BUILDGRID_LOGSTREAM_SERVER="localhost:50051"

check_server_started "50051"

# Buildgrid server with only a Bots service
bgd server start "$BUILDBOX_E2E_DATA_DIR/bgdconfig_standalone_bots.yml" &
BUILDGRID_BOTS_SERVER="localhost:50052"

check_server_started "50052"
