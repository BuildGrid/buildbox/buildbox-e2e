buildbox-e2e
============

This repository contains end-to-end tests for BuildGrid and buildbox.
Tests can be run locally during development and in CI.

Tests cover the following repositories:
- ``BuildGrid/BuildGrid.git``
- ``BuildGrid/BuildGrid-logstream.git``
- ``BuildGrid/buildbox/buildbox.git``
- ``BuildGrid/buildbox/userchroot.git``

Running tests locally
---------------------

To run tests do ``./run.sh``.

Running tests locally against local source code
-----------------------------------------------

To run tests test agaist local repositories, place them under
the directory ``src/`` (e.g.: ``src/buildbox/``).

This can be achieved by either copying source repositories to the
directory ``src/`` at the root of this repo, or bind-mounting into
``/buildbox-e2e/src/`` inside the container.

Adding a new test
-----------------

Test scripts are placed inside ``bin/tests/``. Create a new script in that
subdirectory and add a corresponding ``run_test`` command to invoke it in
``bin/build-and-run-tests.sh``.

The test fixture adds all buildbox components to the ``$PATH`` of the script,
and runs services such as BuildGrid, buildbox-casd and buildbox-worker before
the test starts and shuts them down after the test finishes.

Test scripts must exit with code 0 if successful, non-zero otherwise.

Internals
---------

The logic in the main ``Dockerfile`` is implemented with two Docker images.
One is responsible of building a filesystem where all the buildbox binaries
are available and the other runs the actual tests in an environment created
exclusively for each test.

Dockerfile
``````````

The main Docker image runs integration tests that are implemented by scripts in
``bin/tests/``. It relies on Docker to run each test in its own environment.

.. code-block:: shell
    ### if on windows/mac(intel-based)
    > docker build -t buildbox-e2e .
    # Run all integration tests (for master branches):
    > docker run -v /var/run/docker.sock:/var/run/docker.sock buildbox-e2e

    ### if on mac(M1-based)
    > docker build --platform linux/amd64 -t buildbox-e2e .
    # Run all integration tests (for master branches):
    > docker run --platform linux/amd64 -v /var/run/docker.sock:/var/run/docker.sock buildbox-e2e

Bind mounting the Docker daemon socket allows the ``buildbox-e2e`` container to
talk to it, which is required to build intermediate images and ``docker run``
each test script. Alternatively, ``buildbox-e2e`` can be run in
``--privileged`` mode.

e2e-environment.dockerfile
``````````````````````````
This image carries out, at build time, the steps necessary for building each
project's sources, fetching the ones that weren't provided locally.

.. code-block:: shell

    > docker build --platform linux/amd64 -t e2e-environment -f e2e-environment.dockerfile .

The steps to fetch and builds sources are implemented in
``bin/fetch-sources-and-build.sh``.

e2e-runner.dockerfile
`````````````````````
This image uses ``e2e-environment`` as a base, and contains the test setup and
scripts. It allows invoking ``docker run`` with specific tests cases, providing
a clean environment for each test script.

The initial setup stage is done by ``bin/create-test-environment.sh``, which is
defined as the ``ENTRYPOINT``. All services will be started before executing
the script passed as argument to ``docker run``.

Test scripts can assume that all binaries are available in ``$PATH`` and that
all services are up and running.

.. code-block:: shell

    # Running a single test case:
    > docker build  --platform linux/amd64 -t e2e-runner -f e2e-runner.dockerfile .
    > docker run  --platform linux/amd64 e2e-runner /tests/test-recc-output.sh

CI
````

GitLab pipeline ``.gitlab-ci.yml``, when triggered without additional parameters,
runs the tests against the HEADs of the repositories.
This is useful for testing changes in ``buildbox-e2e`` itself.

Other repositories trigger tests against the specific commit IDs in the
following manner:

.. code-block::yaml

    trigger-e2e:
      variables:
        BUILDBOX_COMMIT: $CI_COMMIT_SHA
      trigger:
        project: "BuildGrid/buildbox/buildbox-e2e"
        strategy: depend

This will trigger a ``buildbox-e2e`` pipeline with specific buildbox commit,
and will wait for the pipeline to finish successfully. For other repositories
their default branches will be checked out.

The following variables can be set on the trigger:
- ``BUILDBOX_COMMIT``
- ``BUILDGRID_COMMIT``
- ``LOGSTREAM_COMMIT``
- ``USERCHROOT_COMMIT``

More that one variable can be specified at the same time, if ever needed.
