FROM docker:dind

RUN apk add --no-cache bash

COPY . /buildbox-e2e
ENV PATH="/buildbox-e2e/bin:${PATH}"

ARG ENV_BASE_IMAGE=registry.gitlab.com/buildgrid/buildbox/buildbox-e2e/buildbox-e2e-base:latest
ENV ENV_BASE_IMAGE=${ENV_BASE_IMAGE}

ENTRYPOINT [ "/buildbox-e2e/bin/build-and-run-tests.sh" ]
