#!/usr/bin/env bash
set -x

main() {
    echo -e "\n\033[1;34m--- Creating test environment for [${@}]...\033[0m"

    echo "buildbox-casd bind location: ${CASD_ENDPOINT:=127.0.0.1:50011}"
    echo "buildbox-casd stage location: ${STAGE_LOCATION:=/builds/buildbox/workspace}"
    echo "buildbox-casd user: ${CASD_USER:=casduser}"
    echo "buildbox-worker user: ${WORKER_USER:=workeruser}"
    echo "BuildGrid server: ${BUILDGRID_SERVER:=localhost:50051}"
    echo "BuildGrid config: ${BUILDGRID_CONFIG:=bgdconfig.yml}"
    echo "bgd-logstream config: ${LOGSTREAM_CONFIG:=logstreamconfig.yml}"
    echo "Distributed BuildGrid config: $DISTRIBUTED_BUILDGRID"
    echo "Run with rumbad: ${RUN_WITH_RUMBAD:=false}"

    create_users

    ulimit -a

    launch_buildgrid
    if [[ $? -ne 0 ]]; then
        echo "Failed to launch BuildGrid"
        exit 1
    fi

    launch_logstream
    if [[ $? -ne 0 ]]; then
        echo "Failed to launch Logstream"
        exit 1
    fi

    wait_for_servers
    if [[ $? -ne 0 ]]; then
        echo "Servers did not start properly"
        exit 1
    fi

    launch_casd
    if [[ $? -ne 0 ]]; then
        echo "Failed to launch buildbox-casd"
        exit 1
    fi

    if [[ -n $BUILDBOX_E2E_DISABLE_WORKER ]]; then
        echo "Disabling worker (cache-only mode)"
    else
        # Select runner:
        buildbox_runner_binary_path=$(which "${BUILDBOX_RUNNER}")
        if [[ ! -x "$buildbox_runner_binary_path" ]]; then
            hosttools_runner_path=$(which buildbox-run-hosttools)
            echo "Path to runner not specified, using buildbox-run-hosttools: $hosttools_runner_path"
            BUILDBOX_RUNNER_BINARY_PATH=${hosttools_runner_path}
        else
            BUILDBOX_RUNNER_BINARY_PATH=${buildbox_runner_binary_path}
            echo "Setting runner binary path to [${BUILDBOX_RUNNER_BINARY_PATH}]"
        fi

        launch_worker
        if [[ $? -ne 0 ]]; then
            echo "Failed to launch buildbox-worker"
            exit 1
        fi
    fi

    if [[ "$RUN_WITH_RUMBAD" == "true" ]]; then
        launch_rumbad
        if [[ $? -ne 0 ]]; then
            echo "Failed to launch rumbad"
            exit 1
        fi
        # Make recc publish to rumbad port, by default it does not publish to any port
        export RECC_COMPILATION_METADATA_UDP_PORT=19111
    fi

    # Give the test script a fixed directory to use as workspace:
    temp_dir="/tmp/test_environment_workspace"
    rm -rf "$temp_dir"
    mkdir -p "$temp_dir"
    cd "$temp_dir" || exit 1

    echo -e "\n\033[1;34m---Running [${@}] inside [$temp_dir]...\033[0m"
    # Give control to the test command passed as argument (preserving
    # all environment variables):
    exec "$@"
}

create_users() {
    CASD_USER_EXISTS=$(
        id -u $CASD_USER >/dev/null 2>&1
        echo $?
    )
    WORKER_USER_EXISTS=$(
        id -u $WORKER_USER >/dev/null 2>&1
        echo $?
    )

    if [[ $CASD_USER_EXISTS -ne 0 ]]; then
        useradd --no-log-init $CASD_USER
    fi

    if [[ $WORKER_USER_EXISTS -ne 0 ]]; then
        useradd --no-log-init $WORKER_USER
    fi
}

launch_buildgrid() {
    echo "Starting BuildGrid..."
    if [[ -z $DISTRIBUTED_BUILDGRID ]]; then
        # We do not set verbose logs here to prevent the CI from exceeding the log size limit.
        # You can add back -vvv if trying to diagnose test failures.
        bgd server start "$BUILDBOX_E2E_DATA_DIR/$BUILDGRID_CONFIG" &
        BUILDGRID_SERVER="localhost:50051"
    else
        source "$BUILDBOX_E2E_DATA_DIR/$DISTRIBUTED_BUILDGRID"
    fi
}

launch_logstream() {
    echo "Starting a redis server"
    redis-server &
    REDIS_SERVER="localhost:6379"

    echo "Starting bgd-logstream..."
    bgd-logstream --server-config="$BUILDBOX_E2E_DATA_DIR/$LOGSTREAM_CONFIG" &
    LOGSTREAM_SERVER="localhost:50055"
}

wait_for_servers() {
    TIMEOUT=40
    INTERVAL=2
    ATTEMPTS=$((TIMEOUT / INTERVAL))
    for ((i = 1; i <= ATTEMPTS; i++)); do
        sleep $INTERVAL
        server_started=0
        if [ "$BUILDGRID_SERVER" = "localhost:50051" ]; then
            netstat -pln | grep 50051 # Check for server existence
            prev_exit_code=$?
            server_started=$((prev_exit_code | server_started))
        fi
        logstream_server_started=0
        # Check if log stream is started or not
        netstat -pln | grep 50055
        prev_exit_code=$?
        logstream_server_started=$((prev_exit_code | logstream_server_started))
        if [ "$server_started" -eq 0 ] && [ "$logstream_server_started" -eq 0 ]; then
            echo "Buildgrid and Logstream Server Started"
            break
        fi
    done
    if [ $server_started -ne 0 ]; then
        echo "Buildgrid server did not start for $TIMEOUT seconds"
        exit 1
    fi
    if [ "$logstream_server_started" -ne 0 ]; then
        echo "Logstream server did not start for $TIMEOUT seconds"
        exit 1
    fi
}

launch_casd() {
    echo "Starting buildbox-casd..."
    BUILDBOX_CASD_LOCAL_CACHE="$(sudo -u $CASD_USER mktemp -d)"

    if [[ -z $DISTRIBUTED_BUILDGRID ]]; then
        CASD_SERVER=$BUILDGRID_SERVER
    else
        CASD_SERVER=$BUILDGRID_CAS_SERVER
    fi

    sudo -u "$CASD_USER" BUILDBOX_STAGER=hardlink -- \
        "$(which buildbox-casd)" \
        --verbose \
        --cas-remote=http://$CASD_SERVER \
        --bind="$CASD_ENDPOINT" \
        "$BUILDBOX_CASD_LOCAL_CACHE" &

    TIMEOUT=10
    INTERVAL=1
    ATTEMPTS=$((TIMEOUT / INTERVAL))
    for ((i = 1; i <= ATTEMPTS; i++)); do
        sleep $INTERVAL
        netstat -pln | grep 50011 && break
        if [ $i -eq $ATTEMPTS ]; then
            echo "buildbox-casd failed to start"
            exit 1
        fi
    done
}

launch_worker() {
    echo "Starting buildbox-worker..."
    if [[ -n $BUILDBOX_E2E_RUNNER_DISABLE_CASD ]]; then
        echo "Runner: Disabling support for staging via casd (fallback stage)"
        DISABLE_CASD_RUNNER_ARG="--runner-arg=--disable-localcas"
    fi

    if [[ -z $DISTRIBUTED_BUILDGRID ]]; then
        BOTS_SERVER=$BUILDGRID_SERVER
    else
        BOTS_SERVER=$BUILDGRID_BOTS_SERVER
    fi

    # Make these paths available from test scripts:
    export LOGSTREAM_STDOUT_FILE="/tmp/logstream.out"
    export LOGSTREAM_STDERR_FILE="/tmp/logstream.err"
    LOGSTREAM_READ_COMMAND="$BUILDBOX_E2E_DATA_DIR/read-logstream.sh $(which bgd) {stdout} $LOGSTREAM_STDOUT_FILE {stderr} $LOGSTREAM_STDERR_FILE"
    # Spawn a reader in the background and redirect its output to a file.
    # buildbox-worker will replace "{}" with the write resource name.

    if [[ "$BUILDBOX_RUNNER" = "buildbox-run-userchroot" ]]; then
        USERCHROOT_BINARY_PATH=$(which userchroot)
        EXTRA_RUNNER_ARG="--runner-arg=--userchroot-bin=$USERCHROOT_BINARY_PATH"

        # userchroot setup
        STAGE_LOCATION_BASE="$(dirname "$STAGE_LOCATION")"
        echo "$CASD_USER:$STAGE_LOCATION_BASE" >>/etc/userchroot.conf
        chmod 0644 /etc/userchroot.conf
        mkdir -p -m 0755 "$STAGE_LOCATION"
        chown -R $CASD_USER:$WORKER_USER "$STAGE_LOCATION_BASE"
    elif [[ "$BUILDBOX_RUNNER" = "buildbox-run-bubblewrap" ]]; then
        EXTRA_RUNNER_ARG="--runner-arg=--enable-tmp-outputs"
        STAGE_LOCATION_BASE="$(dirname "$STAGE_LOCATION")"
        # Run as the same user as buildbox-casd
        WORKER_USER=$CASD_USER
        mkdir -p -m 0777 "$STAGE_LOCATION"
        chown -R $CASD_USER "$STAGE_LOCATION_BASE"
    else
        mkdir -p "$STAGE_LOCATION" && chmod 0777 "$STAGE_LOCATION"
        chown -R $CASD_USER "$STAGE_LOCATION_BASE"
    fi

    sudo -u "$WORKER_USER" -- \
        "$(which buildbox-worker)" \
        --verbose \
        --buildbox-run="$BUILDBOX_RUNNER_BINARY_PATH" \
        --runner-arg=--workspace-path=$STAGE_LOCATION \
        $DISABLE_CASD_RUNNER_ARG \
        $EXTRA_RUNNER_ARG \
        --bots-remote=http://$BOTS_SERVER \
        --cas-remote=http://$CASD_ENDPOINT \
        --logstream-remote=http://$LOGSTREAM_SERVER \
        --launch-logstream-command="$LOGSTREAM_READ_COMMAND" \
        &

    TIMEOUT=10
    INTERVAL=1
    ATTEMPTS=$((TIMEOUT / INTERVAL))
    for ((i = 1; i <= ATTEMPTS; i++)); do
        sleep $INTERVAL
        pgrep -u "$WORKER_USER" buildbox-worker && break
        if [ $i -eq $ATTEMPTS ]; then
            echo "buildbox-worker failed to start"
            ps aux | grep buildbox-worker
            exit 1
        fi
    done
}

launch_rumbad() {
    echo "Starting rumbad..."
    # Make this path available from test scripts:
    export RUMBAD_OUTPUT_FILE="/tmp/rumbad.out"

    # Use a short publish interval to avoid waiting in tests and write to file
    # to allow easy inspection of published data
    RUMBAD_PUBLISH_INTERVAL=1 $(which rumbad) 19111 >${RUMBAD_OUTPUT_FILE} &

    TIMEOUT=5
    INTERVAL=1
    ATTEMPTS=$((TIMEOUT / INTERVAL))
    for ((i = 1; i <= ATTEMPTS; i++)); do
        sleep $INTERVAL
        pgrep rumbad && break
        if [ $i -eq $ATTEMPTS ]; then
            echo "rumbad failed to start"
            ps aux | grep rumbad
            exit 1
        fi
    done
}

main "$@"
