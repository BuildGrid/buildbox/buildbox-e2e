#!/usr/bin/env bash
set -e

# Builds every buildbox project for testing.
#
# Unless pointed to a source directory with $BUILDBOX_{component}_SOURCE_ROOT,
# fetches the contents of the master branch for that component.
#
# Arguments:
#   - $BUILDBOX_E2E_ROOT: the directory where everything is stored.
# Output:
#   - $BUILDBOX_E2E_ROOT/bin/ contains symlinks to the built binaries.

checkout()
{
  SOURCE_ROOT="$1"
  REPO="$2"
  COMMIT="$3"

  if ! [[ -d "$SOURCE_ROOT" ]]; then
    echo "Downloading $REPO..."
    case "$COMMIT" in
      HEAD)
	echo "Checking out default branch"
	git clone --depth 1 "https://gitlab.com/BuildGrid/$REPO.git" "$SOURCE_ROOT"
	;;
      "")
	echo "Empty commit ID passed to checkout, expected either SHA commit ID or 'HEAD'" >&2
	exit 1
	;;
      *)
	echo "Checking out $COMMIT"
	git clone "https://gitlab.com/BuildGrid/$REPO.git" "$SOURCE_ROOT"
	(cd "$SOURCE_ROOT" && git checkout "$COMMIT")
	;;
    esac
  fi
}

fetch_sources()
{
    echo -e "\n\n\e[1;32m--- Fetching sources...\e[0m\n"

    # Unless an environment variable specifies where to find the sources,
    # fetch the master branch of every project:

    checkout "$BUILDBOX_SOURCE_ROOT" buildbox/buildbox "$BUILDBOX_COMMIT"
    checkout "$BUILDGRID_SOURCE_ROOT" buildgrid "$BUILDGRID_COMMIT"
    checkout "$LOGSTREAM_SOURCE_ROOT" buildgrid-logstream "$LOGSTREAM_COMMIT"
    checkout "$CHROOTBUILDER_SOURCE_ROOT" buildbox/chrootbuilder "$CHROOTBUILDER_COMMIT"
    checkout "$USERCHROOT_SOURCE_ROOT" buildbox/userchroot "$USERCHROOT_COMMIT"
}

main()
{
    source env-vars.sh
    set_and_print_env_variables

    fetch_sources
}

main
