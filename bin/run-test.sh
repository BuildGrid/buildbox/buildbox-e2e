#!/usr/bin/env bash
set -e

STARTTIME=$SECONDS
SCRIPT=$1
shift

INFOCMD="docker run $SCRIPT $@"
echo -e "\033[1;1m---Running [$INFOCMD].\033[0m"

LOG=$(mktemp)
cleanup() {
  rm -f $LOG
}
trap cleanup EXIT

if docker run --rm "$@" e2e-runner /tests/$SCRIPT >$LOG 2>&1; then
  echo -e "\033[1;32m---[$INFOCMD] succeeded ($((SECONDS-$STARTTIME))s).\033[0m"
  exit 0
fi

cat "$LOG"

echo -e "\033[1;31m---[$INFOCMD] failed.\033[0m"
exit 1
