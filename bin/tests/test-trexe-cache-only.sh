#!/usr/bin/env bash
set -e

# Tests local execution with remote caching using trexe.

TREXE_CMD="trexe --cache-only --buildbox-run=$BUILDBOX_RUNNER --cas-remote=http://$CASD_ENDPOINT --ac-remote=http://$BUILDGRID_SERVER"

echo hello world > hello.txt

# Test basic execution and stdout

$TREXE_CMD /bin/echo hello world > stdout
if [[ "$(cat stdout)" = "$(cat hello.txt)" ]];
then
    echo "Correct stdout"
else
    echo "Incorrect stdout: " $(cat stdout)
    exit 1
fi


# Test support of input and output files and more complex command lines

mkdir input
cp hello.txt input/

$TREXE_CMD --input-path=input --d=output --output-path=hello.b64 /bin/sh -c 'base64 <hello.txt >hello.b64'
if [[ "$(base64 <hello.txt)" = "$(cat output/hello.b64)" ]];
then
    echo "Correct output file"
else
    echo "Incorrect output file: " $(cat output/hello.b64)
    exit 1
fi

# Test support of multiple inputs

mkdir input2
echo goodbye world > goodbye.txt

cat hello.txt goodbye.txt > hellogoodbye.txt

## Ensure files are not in cwd
mv hello.txt input/
mv goodbye.txt input2/

$TREXE_CMD --input-path=./input --input-path=input2 --d=output --output-path=hellogoodbye.txt /bin/sh -c '/bin/cat hello.txt goodbye.txt > hellogoodbye.txt'
if [[ "$(cat hellogoodbye.txt)" = "$(cat output/hellogoodbye.txt)" ]];
then
    echo "Correct output file"
else
    echo "Incorrect output file: " $(cat output/hellogoodbye.txt)
    exit 1
fi

mkdir input/subdir
mkdir input2/subdir
echo foo > input/subdir/foo.txt
echo bar > input2/subdir/bar.txt

cat input/subdir/foo.txt input2/subdir/bar.txt > foobar.txt
$TREXE_CMD --input-path=input --input-path=./input2/subdir --d=output --output-path=foobar.txt /bin/sh -c '/bin/cat subdir/foo.txt bar.txt > foobar.txt'
if [[ "$(cat foobar.txt)" = "$(cat output/foobar.txt)" ]];
then
    echo "Correct output file"
else
    echo "Incorrect output file: " $(cat output/foobar.txt)
    exit 1
fi

echo -e "\033[1;32m---[$0] succeeded.\033[0m"
