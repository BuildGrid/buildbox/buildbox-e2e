#!/usr/bin/env bash
set -e

# This script sets up two casd instances and tests that data is consistent
# performing different operations. It will test transfers to/from the remote as
# well as via the proxy.
#
#  * proxy.sock                       * server.sock
#  |                                  |
#  ------> [ Proxy instance ] <--------------> [ Remote instance ]
#                   |                                    |
#            CACHES_DIR/proxy                    CACHES_DIR/server

main() {
  INPUT_DIRECTORY="$BUILDBOX_E2E_DATA_DIR/casd-integration-data"

  launch_casd_instances
  run_integration_test
}

function run_integration_test() {
  DATA_DIR=$(mktemp -d)

  cp -a $INPUT_DIRECTORY $DATA_DIR
  curl "https://ftp.gnu.org/gnu/hello/hello-2.1.0.tar.gz" | tar -C $DATA_DIR -xz
  dd if=/dev/zero of=$DATA_DIR/zeroes.blob bs=1M count=16
  dd if=/dev/urandom of=$DATA_DIR/16M.blob bs=1M count=16
  dd if=/dev/urandom of=$DATA_DIR/64M.blob bs=1M count=64
  dd if=/dev/urandom of=$DATA_DIR/256M.blob bs=1M count=256
  echo "Hello!" > $DATA_DIR/file
  ln --symbolic --force file $DATA_DIR/file.txt

  # Uploading directory to remote:
  DIRECTORY_DIGEST=$(cli_uploader "$REMOTE_ENDPOINT" "$DATA_DIR") || exit 1

  # Downloading from remote:
  REMOTE_OUTPUT_DIR="$DOWNLOADS_DIR/remote-output"
  mkdir "$REMOTE_OUTPUT_DIR"
  cli_downloader "$REMOTE_ENDPOINT" "$DIRECTORY_DIGEST" "$REMOTE_OUTPUT_DIR" || exit 1
  directories_match "$DATA_DIR" "$REMOTE_OUTPUT_DIR"

  # Staging from remote:
  REMOTE_STAGE_DIRECTORY="$DOWNLOADS_DIR/remote-stage"
  nohup cli_stager "$REMOTE_ENDPOINT" "$DIRECTORY_DIGEST" "$REMOTE_STAGE_DIRECTORY" &
  sleep 5  # Delay to make sure that the whole tree is staged (*)
  directories_match "$DATA_DIR" "$REMOTE_STAGE_DIRECTORY"
  killall cli_stager

  # Staging from proxy:
  PROXY_STAGE_DIRECTORY="$DOWNLOADS_DIR/proxy-stage"
  nohup cli_stager "$PROXY_ENDPOINT" "$DIRECTORY_DIGEST" "$PROXY_STAGE_DIRECTORY" &
  sleep 10   # Delay to make sure that the whole tree is staged (*)
  directories_match "$DATA_DIR" "$PROXY_STAGE_DIRECTORY"
  killall cli_stager

  # (*) Those values seem to currently be suitable on the GitLab CI, but are arbitrary.

  # Downloading from proxy:
  PROXY_OUTPUT_DIR="$DOWNLOADS_DIR/proxy-output"
  mkdir "$PROXY_OUTPUT_DIR"
  cli_downloader "$PROXY_ENDPOINT" "$DIRECTORY_DIGEST" "$PROXY_OUTPUT_DIR" || exit 1
  directories_match "$DATA_DIR" "$PROXY_OUTPUT_DIR"

  # The stage directories didn't originally exist, so we expect `GetTree()` to delete them:
  assert_stage_directory_deleted "$PROXY_STAGE_DIRECTORY"
  assert_stage_directory_deleted "$REMOTE_STAGE_DIRECTORY"
}


function directories_match() {
  directories_contents_match "$1" "$2"
  directories_contain_same_executable_files "$1" "$2"
}

function directories_contents_match() {
  (diff -r "$1" "$2") \
  || (echo "Contents of directories \"$1\" and \"$2\" do not match"; exit 1)
}

function directories_contain_same_executable_files() {
  (diff <(echo $(executable_files $1)) <(echo $(executable_files $2))) \
  || (echo "Executable files in directories \"$1\" and \"$2\" do not match"; exit 1)
}

function executable_files() {
  find "$1" -executable -printf "%P\n" | sort;  # (Print paths relative to $1)
}

function assert_stage_directory_deleted() {
  if [ -d $1 ]; then
   echo "$1 was not deleted after staging"
   exit 1
  fi
}

function launch_casd_instances() {
  # Endpoints:
  SOCKETS_PATH=$(mktemp -d)
  PROXY_ENDPOINT="unix:$SOCKETS_PATH/proxy.sock"
  REMOTE_ENDPOINT="unix:$SOCKETS_PATH/server.sock"

  # Cache paths:
  CACHES_DIR=$(mktemp -d)
  DOWNLOADS_DIR=$(mktemp -d)

  # Launching remote server:
  echo -n "Launching remote instance... "
  BUILDBOX_STAGER=hardlink nohup buildbox-casd --verbose --bind="$REMOTE_ENDPOINT" "$CACHES_DIR/server" &
  sleep 5  # Delay to make sure the buildbox-casd server process is ready
  echo "done."

  # Launching proxy:
  echo -n "Launching proxy instance... "
  BUILDBOX_STAGER=hardlink nohup buildbox-casd --verbose --bind="$PROXY_ENDPOINT" --cas-remote="$REMOTE_ENDPOINT" "$CACHES_DIR/proxy" &
  sleep 5  # Delay to make sure the buildbox-casd proxy process is ready
  echo "done."
}

main $@

echo -e "\033[1;32m---[$0] succeeded.\033[0m"
