#!/usr/bin/env bash
set -e

# Issues a simple compile command using recc in cache-only mode and verifies
# that the obtained binary executes successfully.
#
# Arguments:
#   * $1: Value to specify in RECC_REAPI_VERSION. Valid: {"2.0", "2.1", "2.2"}.

# Populating input directory:
symlinked_header_file="symlinked_test_header_file.h"
header_file="test_header_file.h"
cp "$BUILDBOX_E2E_DATA_DIR/hello.cpp" .
cp "$BUILDBOX_E2E_DATA_DIR/$header_file" .

if [[ ! -L "$symlinked_header_file" ]];
then
    ln --symbolic "$header_file" "$symlinked_header_file"
fi

# recc config:
export RECC_SERVER=$BUILDGRID_SERVER
export RECC_VERBOSE=1
export RECC_FORCE_REMOTE=1
export RECC_CACHE_ONLY=1
export RECC_CACHE_UPLOAD_LOCAL_BUILD=1
export RECC_OUTPUT_FILES_OVERRIDE="hello"
export RECC_DEPS_OVERRIDE="hello.cpp,symlinked_test_header_file.h"
export RECC_INSTANCE=""
if [[ $# -eq 1 ]]; then
    export RECC_REAPI_VERSION="$1"
    echo "Set RECC_REAPI_VERSION=$RECC_REAPI_VERSION"
fi
export RUMBA_VERBOSE=1
export RUMBA_USE_RECC=1

if [ -n "$BUILDBOX_RUNNER" ];
then
    export RECC_RUNNER_COMMAND="$BUILDBOX_RUNNER"
    export RECC_CAS_SERVER="$CASD_ENDPOINT"
    export RECC_ACTION_CACHE_SERVER="$BUILDGRID_SERVER"
fi

# Create tiny gcc wrapper to allow checking whether the action was executed locally.
gcc_path="/usr/bin/gcc-0"
cat > "$gcc_path" << "EOF"
#!/bin/sh
touch gcc.executed
exec gcc "$@"
EOF
chmod a+x "$gcc_path"

rm -f hello gcc.executed

export RECC_OUTPUT_FILE=$(mktemp)

echo "Invoking recc in cache-only mode with cold action cache"
recc "$gcc_path" -I"$(pwd)" hello.cpp -o hello 2>&1 | tee $RECC_OUTPUT_FILE

echo "recc output directory '$(pwd)': $(ls -l)"

if [ -n "$BUILDBOX_RUNNER" ] && grep -q "Launching runner process" $RECC_OUTPUT_FILE;
then
    echo "Action executed in local runner in cache-only mode"
elif [ -z "$BUILDBOX_RUNNER" ] && [ -e gcc.executed ];
then
    echo "Action executed locally in cache-only mode"
else
    echo "ERROR: Action not executed locally in cache-only mode with empty cache"
    exit 1
fi

if [[ "$(./hello)" = "hello_world" ]];
then
    echo "Correct recc output in cache-only mode"
else
    echo "ERROR: Incorrect recc output in cache-only mode"
    exit 1
fi

rm -f hello gcc.executed

echo "Invoking recc in cache-only mode with warm action cache"
recc "$gcc_path" -I"$(pwd)" hello.cpp -o hello 2>&1 | tee $RECC_OUTPUT_FILE

if [[ "$(./hello)" = "hello_world" ]];
then
    if grep -q "Action Cache hit" $RECC_OUTPUT_FILE;
    then
        echo "Correct cached recc output in cache-only mode"
    else
        echo "ERROR: Unexpected cache miss in cache-only mode"
        exit 1
    fi
else
    echo "ERROR: Incorrect cached recc output in cache-only mode"
    exit 1
fi

rm -f hello gcc.executed

echo -e "\033[1;32m---[$0] succeeded.\033[0m"
