#!/usr/bin/env bash
set -e

mkdir -p out/test-casd-metrics

export RUN_DURATION_SECS=5
timeout --preserve-status $RUN_DURATION_SECS buildbox-casd --metrics-mode=stderr --metrics-publish-interval=1 out/test-casd-metrics/cache 2> out/test-casd-metrics/metrics.out || exit 1
test $(grep "buildbox Metrics:" out/test-casd-metrics/metrics.out | wc -l) -eq $RUN_DURATION_SECS

echo -e "\033[1;32m---[$0] succeeded.\033[0m"
