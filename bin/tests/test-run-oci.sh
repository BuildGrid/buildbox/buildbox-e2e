#!/usr/bin/env bash
set -e

# Tests buildbox-run-oci works

echo "Uploading contents of $BUILDBOX_E2E_DATA_DIR/oci-mock-data"
casupload --cas-server=http://$BUILDGRID_SERVER $BUILDBOX_E2E_DATA_DIR/oci-mock-data

rexplorer \
  --action=$(cat $BUILDBOX_E2E_DATA_DIR/oci-mock-data/outputs/action.digest) \
  --verbose \
  --pretty \
  --remote=http://$BUILDGRID_SERVER

echo "Running buildbox-run-oci:"
buildbox-run-oci \
  --action=$BUILDBOX_E2E_DATA_DIR/oci-mock-data/outputs/action.data \
  --remote=http://$CASD_ENDPOINT \
  --verbose \
  --no-logs-capture \
  --log-level=debug \
  --action-result=/tmp/result

echo -e "\033[1;32m---[$0] succeeded.\033[0m"
