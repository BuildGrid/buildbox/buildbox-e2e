#!/usr/bin/env bash
set -e

## Check empty file in cache

mkdir -p input/fuse-empty/mount
buildbox-fuse --local=$BUILDBOX_E2E_DATA_DIR/fuse-integration-data --input-digest-value="19796dd6150649962f6cea1d3ce43eec7a866dc3bb7c70cc0891618223469411/75" input/fuse-empty/mount&
PID=$!

sleep 1

ls -R input

if [[ ! -r input/fuse-empty/mount/foo || $(cat input/fuse-mount/mount/foo) != "" ]]; then
  echo "Expected input/fuse-mount/mount/foo to be an empty file"
  exit 1
fi

kill $PID
