#!/usr/bin/env bash
set -e

# Tests the output of the `grpc_cli` utility when issuing a `ListOperations()`
# request.

main()
{
    echo "[$0] Testing grpc_cli..."

    # recc's standard outputs are saved to this file to later recover
    # the ID of the Operation:
    export RECC_OUTPUT_FILE=$(mktemp)
    run_recc_job

    test_grpc_cli
    echo "[$0] grpc_cli test succeeded."
}


run_recc_job()
{
    echo "Running recc job to create an Operation in the remote..."

    # Populating input directory:
    symlinked_header_file="symlinked_test_header_file.h"
    header_file="test_header_file.h"
    cp "$BUILDBOX_E2E_DATA_DIR/hello.cpp" .
    cp "$BUILDBOX_E2E_DATA_DIR/$header_file" .

    if [[ ! -L "$symlinked_header_file" ]];
    then
        ln --symbolic "$header_file" "$symlinked_header_file"
    fi

    # recc config:
    export RECC_SERVER=$BUILDGRID_SERVER
    export RECC_VERBOSE=1
    export RECC_FORCE_REMOTE=1
    export RECC_OUTPUT_FILES_OVERRIDE="hello"
    export RECC_DEPS_OVERRIDE="hello.cpp,symlinked_test_header_file.h"
    export RECC_INSTANCE=""

    # Issuing the remote execution request:
    (
        gcc_path=$(which gcc)
        if ! recc "$gcc_path" -I"$(pwd)" hello.cpp -o hello 2>&1 | tee -a "$RECC_OUTPUT_FILE";
        then
            echo "recc failed, could not generate Operation."
            exit 1
        else
            echo "recc succeeded."
        fi
    )

}

test_grpc_cli()
{
    # Parse the output to grab an operation ID
    # 2021-04-01T19:15:56.706+0000 [16388:140215078352640] [remoteexecutionclient.cpp:74] [DEBUG] Waiting for Operation: /7be8109c-25f8-4633-ae38-d74ac7aedab5
    if ! grep -q "Waiting for Operation:" "$RECC_OUTPUT_FILE";
    then
        echo "Did not find operation ID in $RECC_OUTPUT_FILE, cannot run test on grpc_cli"
        exit 1
    fi

    OPERATION_ID=$(grep "Waiting for Operation:" "$RECC_OUTPUT_FILE" | head -1 | awk '{print $NF}')
    echo "recc created Operation [$OPERATION_ID]"

    command="GetOperation"
    echo "Testing grpc_cli with $command"
    local grpc_output
    grpc_output=$(grpc_cli call "$BUILDGRID_SERVER" --json_output "$command" "name: '$OPERATION_ID'")

    # Confirm that we find "executionMetadata" in the output
    if ! echo "$grpc_output" | grep -q "executionMetadata";
    then
        printf "Did not find executionMetadata in output of grpc_cli:\n%s\n" "$grpc_output"
        exit 1
    else
        printf "Successfully invoked grpc_cli, output is\n%s\n" "$grpc_output"
    fi
}

main
