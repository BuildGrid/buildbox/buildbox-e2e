#!/usr/bin/env bash
set -e

# Upload a directory with casupload, fetch it back with casdownload, and
# assert that no data was lost.

output_directory=$(mktemp -d)

# Try uploading and downloading a directory using casupload/casdownload
echo "Uploading contents of $BUILDBOX_E2E_DATA_DIR"
casupload --cas-server=http://$BUILDGRID_SERVER --output-digest-file="data.digest" $BUILDBOX_E2E_DATA_DIR/

echo "Downloading directory back:"
casdownload --instance='' --cas-server=http://$BUILDGRID_SERVER --root-digest="$(cat data.digest)" --destination-dir="$output_directory" || exit 1

# Check that downloaded directory matches what we uploaded:
diff --brief --recursive $BUILDBOX_E2E_DATA_DIR "$output_directory" || exit 1
echo "Uploaded and downloaded directories match"
echo -e "\033[1;32m---[$0] succeeded.\033[0m"
