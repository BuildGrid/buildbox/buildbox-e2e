#!/usr/bin/env bash
set -e

# Tests whether gRPC setup is complete for a standalone Bots service

echo "[$0] Testing gRPC setup"

export EXECUTE_OUTPUT_FILE=$(mktemp)

bgd execute --remote-cas http://localhost:50051 command $BUILDBOX_E2E_DATA_DIR ls | tee -a "$EXECUTE_OUTPUT_FILE"

# Check if execute command completed and ExecuteResponse contains a response value
if grep -q "done: true" "$EXECUTE_OUTPUT_FILE"; then
    TYPE_URL="grep 'type_url' "$EXECUTE_OUTPUT_FILE" | tail -1"
    VALUE="grep 'value' "$EXECUTE_OUTPUT_FILE" | tail -1"
    VALUE_LEN=$(eval "$VALUE")

    if ! [[ $(eval "$TYPE_URL")  == *"ExecuteResponse"* && ${#VALUE_LEN} -gt 10 ]] ; then
        echo "gRPC setup test failed"
        exit 1
    fi
else
    echo "gRPC setup test failed"
    exit 1
fi

echo -e "\033[1;32m---[$0] succeeded.\033[0m"
