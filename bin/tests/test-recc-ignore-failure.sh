#!/usr/bin/env bash
set -e

# Issues a simple compile command using recc in cache-only mode and verifies
# that recc correctly executes before and after ignoring the failure results.
#
# Arguments:
#   * $1: Value to specify in RECC_REAPI_VERSION. Valid: {"2.0", "2.1", "2.2"}.

# Populating input directory:
symlinked_header_file="symlinked_test_header_file.h"
header_file="test_header_file.h"
cp "$BUILDBOX_E2E_DATA_DIR/hello.cpp" .
cp "$BUILDBOX_E2E_DATA_DIR/$header_file" .

if [[ ! -L "$symlinked_header_file" ]];
then
    ln --symbolic "$header_file" "$symlinked_header_file"
fi

# recc config:
export RECC_SERVER=$BUILDGRID_SERVER
export RECC_VERBOSE=1
export RECC_FORCE_REMOTE=1
export RECC_CACHE_ONLY=1
export RECC_CACHE_UPLOAD_LOCAL_BUILD=1
export RECC_OUTPUT_FILES_OVERRIDE="hello"
export RECC_DEPS_OVERRIDE="hello.cpp,symlinked_test_header_file.h"
export RECC_INSTANCE=""

if [[ $# -eq 1 ]]; then
    export RECC_REAPI_VERSION="$1"
    echo "Set RECC_REAPI_VERSION=$RECC_REAPI_VERSION"
fi
export RUMBA_VERBOSE=1
export RUMBA_USE_RECC=1

if [ -n "$BUILDBOX_RUNNER" ];
then
    export RECC_RUNNER_COMMAND="$BUILDBOX_RUNNER"
    export RECC_CAS_SERVER="$CASD_ENDPOINT"
    export RECC_ACTION_CACHE_SERVER="$BUILDGRID_SERVER"
fi

export RECC_OUTPUT_FILE=$(mktemp)
export RECC_CACHE_UPLOAD_FAILED_BUILD=1

gcc_path="/usr/bin/gcc-0"
cat > "$gcc_path" << "EOF"
#!/usr/bin/env bash
if [[ "$@" == *"hello.cpp -o hello"* ]]; then
    touch hello
    exit 1
fi
exec gcc "$@"
EOF
chmod a+x "$gcc_path"

# Executing recc to populate the cache
recc "$gcc_path" -I"$(pwd)" hello.cpp -o hello 2>&1 | tee $RECC_OUTPUT_FILE

if grep -q -e "Action not cached and running in cache-only mode" -e "Executing action in local runner" $RECC_OUTPUT_FILE;
then
    echo "Correct execution locally"
else
    echo "ERROR: Action not executed locally"
    exit 1
fi

# Executing recc to get cache hit with ignore failure turned off
recc "$gcc_path" -I"$(pwd)" hello.cpp -o hello 2>&1 | tee $RECC_OUTPUT_FILE

if grep -q "Action Cache hit" $RECC_OUTPUT_FILE;
then
    echo "Correct cached recc output in cache-only mode"
else
    echo "ERROR: Unexpected cache miss in cache-only mode before ignoring failures"
    exit 1
fi

export RECC_OUTPUT_FILE=$(mktemp)
export RECC_ACTION_SALT="new salt"

export RECC_IGNORE_FAILURE_RESULT=1

# Execute recc to get cache miss with ignore failure turned on
recc "$gcc_path" -I"$(pwd)" hello.cpp -o hello 2>&1 | tee $RECC_OUTPUT_FILE

if grep -q "Action Cache hit" $RECC_OUTPUT_FILE;
then
    echo "ERROR: Unexpected cache hit in cache-only mode after ignoring failures"
    exit 1
else
    echo "Correct cached recc output in cache-only mode"
fi

rm -f hello gcc.executed

echo -e "\033[1;32m---[$0] succeeded.\033[0m"
