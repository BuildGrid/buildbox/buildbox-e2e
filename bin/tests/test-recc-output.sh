#!/usr/bin/env bash
set -e

# Issues a simple compile command using recc and verifies that the obtained
# binary executes successfully.
#
# Arguments:
#   * $1: Value to specify in RECC_REAPI_VERSION. Valid: {"2.0", "2.1", "2.2"}.

# Populating input directory:
symlinked_header_file="symlinked_test_header_file.h"
header_file="test_header_file.h"
cp "$BUILDBOX_E2E_DATA_DIR/hello.cpp" .
cp "$BUILDBOX_E2E_DATA_DIR/$header_file" .

if [[ ! -L "$symlinked_header_file" ]];
then
    ln --symbolic "$header_file" "$symlinked_header_file"
fi

# recc config:
export RECC_SERVER=$BUILDGRID_SERVER
export RECC_VERBOSE=1
export RECC_FORCE_REMOTE=1
export RECC_OUTPUT_FILES_OVERRIDE="hello"
export RECC_DEPS_OVERRIDE="hello.cpp,symlinked_test_header_file.h"
export RECC_INSTANCE=""
if [[ $# -eq 1 ]]; then
    export RECC_REAPI_VERSION="$1"
    echo "Set RECC_REAPI_VERSION=$RECC_REAPI_VERSION"
fi
export RUMBA_VERBOSE=1
export RUMBA_USE_RECC=1

if [[ "$BUILDBOX_RUNNER" != "buildbox-run-hosttools" ]];
then
    # A chroot image is required when running in a sandboxed environment.
    # Pull alpine image, build chroot and upload it to CAS.
    DOCK_IMG="frolvlad/alpine-gxx@sha256:a5f206ffb5b4ef2d66dd04128b62c9b3decefb4cdb1931bdfc0f4b987e9a558f"
    docker pull $DOCK_IMG
    chrootbuilder --docker-timeout 300 --extract $DOCK_IMG /alpine
    casupload --output-digest-file=chroot.digest --cas-server=http://$BUILDGRID_SERVER /alpine

    export RECC_REMOTE_PLATFORM_chrootRootDigest=$(cat chroot.digest)

    # The root directory is read-only with buildbox-run-userchroot.
    # Work in a subdirectory.
    export RECC_WORKING_DIR_PREFIX="workdir"
fi

# Issuing the remote execution request:
# a -v is added to ensure that the command generates
# some output to stderr to validate that's downloaded
export RECC_OUTPUT_FILE=$(mktemp)
gcc_path=$(which gcc)
recc "$gcc_path" -I"$(pwd)" -v -static hello.cpp -o hello 2>&1 | tee -a $RECC_OUTPUT_FILE

echo "recc output directory '$(pwd)': $(ls -l)"

if [[ "$(./hello)" = "hello_world" ]];
then
    echo "Correct recc output"
else
    echo "Incorrect recc output: $(./hello)"
    exit 1
fi

# Test that casdownload , using --action-result, generates all expected output files
# Uses output of recc request to obtain the Action Digest, used in casdownload command
if [[ "$TEST_CASDOWNLOAD" == "true" ]];
then
    # Determine Action Digest from Recc output
    input=$RECC_OUTPUT_FILE
    while IFS= read -r line
    do
        digestline=$(echo $line | grep -Po 'actionDigest=\K[^>]*(?=\])' || :)
        if [ -n "$digestline" ]; then
            action_digest=$digestline
        fi
    done < "$input"

    # Ensure an action digest is set
    if [ -z "$action_digest" ]; then
        echo "No action digest found in recc output"
        exit 1
    fi

    # Use Action Digest to perform a casdownload and check outputs
    # This runs as a non-root user to make permission issues fail
    export CASDOWNLOAD_OUTPUT=$(sudo -u nobody mktemp -d)
    CASDOWNLOAD_PATH=$(which casdownload)
    sudo -u nobody $CASDOWNLOAD_PATH --instance="" --cas-server=http://$BUILDGRID_SERVER --action-digest=$action_digest --destination-dir="$CASDOWNLOAD_OUTPUT" || exit 1

    # Create directory name where metadata, stderr, stdout and exit_code are stored
    output_directory=$(echo $action_digest | cut -f1 -d"/")"-out"

    # Check output file is created
    echo "casdownload output directory: $(ls -al "$CASDOWNLOAD_OUTPUT")"
    cd "$CASDOWNLOAD_OUTPUT" || exit 1
    if test -e "hello"; then
        echo "Output file is generated"
    else
        echo "The generated output file (hello) is missing"
        exit 1
    fi

    # Check that all expected additional outputs exist
    echo "casdownload additional files: $(ls -al "$CASDOWNLOAD_OUTPUT/$output_directory")"
    cd "$output_directory" || exit 1
    if test -e "metadata" && test -e "stderr" && test -e "stdout" && test -e "exit_code"; then
        echo "All additional files generated"
    else
        echo "At least one of the additional files is missing"
        exit 1
    fi
    if test -s "stderr"; then
        echo "stderr was properly downloaded"
    else
        echo "stderr is empty when it should have content"
        exit 1
    fi
fi

echo -e "\033[1;32m---[$0] succeeded.\033[0m"
