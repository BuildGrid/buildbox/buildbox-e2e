#!/usr/bin/env bash
set -e

# Fix exit code when piping commands to `tee`
set -o pipefail

# recc config:
export RECC_SERVER=$BUILDGRID_SERVER
export RECC_VERBOSE=1
export RECC_INSTANCE=""
export RECC_NO_PATH_REWRITE=1
# CMake uses absolute paths
export RECC_DEPS_GLOBAL_PATHS=1
# This enables use of clang-scan-deps
export RECC_COMPILATION_DATABASE=compile_commands.json

# A chroot image is required when running in a sandboxed environment.
# Build chroot and upload it to CAS. Exclude /usr/include and /usr/local/include to prevent conflicts
# with the recc input tree.

TEST_DATA_DIR=$BUILDBOX_E2E_DATA_DIR/recc-clang-scan-deps

docker build --platform linux/amd64 -t recc-clang-scan-deps-test -f "$TEST_DATA_DIR"/Dockerfile "$TEST_DATA_DIR"
chrootbuilder --docker-timeout 300 --extract recc-clang-scan-deps-test /debian
rm -rf /debian/usr/include /debian/usr/local/include

casupload --output-digest-file=chroot.digest --cas-server=http://"$BUILDGRID_SERVER" /debian

export RECC_REMOTE_PLATFORM_chrootRootDigest=$(cat chroot.digest)

# Add clang-scan-deps to the PATH
export PATH=/usr/lib/llvm-14/bin:"$PATH"
export CLANG_SCAN_DEPS=/usr/lib/llvm-14/bin/clang-scan-deps

# Build Hello World using recc
# Test with both make and ninja as cmake processes subdirectories differently
# (changes working directory to subdirectory for make but not for ninja).

TEST_TMP_DIR=$(mktemp -d)
cleanup() {
    rm -rf "$TEST_TMP_DIR"
}
trap cleanup EXIT

cp -a "$TEST_DATA_DIR" "$TEST_TMP_DIR"/make
mkdir -p "$TEST_TMP_DIR"/make/build
cd "$TEST_TMP_DIR"/make/build

if ! cmake .. -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_C_COMPILER_LAUNCHER=$(which recc) -DCMAKE_CXX_COMPILER_LAUNCHER=$(which recc); then
    echo "cmake failed for make"
    exit 1
fi

if ! make -j $(nproc) VERBOSE=1 |& tee make.log; then
    echo "make failed"
    exit 1
fi

if grep -q "Using clang-scan-deps to get dependencies" make.log && ! grep -q "Falling back to dependencies command" make.log; then
    echo "Build with recc + clang-scan-deps successful for cmake with make"
else
    echo "Build with recc + clang-scan-deps failed for cmake with make"
    exit 1
fi

touch "$TEST_TMP_DIR"/make/src/main.c

if ! make -j $(nproc) VERBOSE=1 |& tee make.log; then
    echo "make failed after touching the file"
    exit 1
fi
if grep -q "Running clang-scan-deps on individual translation unit" make.log && ! grep -q "Falling back to dependencies command" make.log; then
    echo "Build with recc + clang-scan-deps incremental successful for cmake with make"
else
    echo "Build with recc + clang-scan-deps failed for cmake with make"
    exit 1
fi

cp -a "$TEST_DATA_DIR" "$TEST_TMP_DIR"/ninja
mkdir -p "$TEST_TMP_DIR"/ninja/build
cd "$TEST_TMP_DIR"/ninja/build

if ! cmake .. -G Ninja -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_C_COMPILER_LAUNCHER=$(which recc) -DCMAKE_CXX_COMPILER_LAUNCHER=$(which recc); then
    echo "cmake failed for ninja"
    exit 1
fi

if ! ninja -v |& tee ninja.log; then
    echo "ninja failed"
    exit 1
fi

if grep -q "Using clang-scan-deps to get dependencies" ninja.log && ! grep -q "Falling back to dependencies command" ninja.log; then
    echo "Build with recc + clang-scan-deps successful for cmake with ninja"
else
    echo "Build with recc + clang-scan-deps failed for cmake with ninja"
    exit 1
fi

touch "$TEST_TMP_DIR"/ninja/src/main.c

if ! ninja -v |& tee ninja.log; then
    echo "ninja failed after touching the file"
    exit 1
fi
if grep -q "Running clang-scan-deps on individual translation unit" ninja.log && ! grep -q "Falling back to dependencies command" ninja.log; then
    echo "Build with recc + clang-scan-deps incremental successful for cmake with ninja"
else
    echo "Build with recc + clang-scan-deps failed for cmake with ninja"
    exit 1
fi

echo -e "\033[1;32m---[$0] succeeded.\033[0m"
