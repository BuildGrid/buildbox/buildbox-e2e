#!/usr/bin/env bash
# No need to set -e: the test checks the exit code from bb-worker

# Launch buildbox-worker with an invalid runner argument and assert that it
# detects it by exiting.

timeout --signal=KILL 10 \
    buildbox-worker \
    --verbose \
    --bots-remote=http://$BUILDGRID_SERVER \
    --cas-remote=http://$CASD_ENDPOINT \
    --buildbox-run=buildbox-run-hosttools \
    --runner-arg=--runner-does-not-support-this-option

EXIT_CODE=$?
if [[ $EXIT_CODE -eq 124 ]]; then
    echo "buildbox-worker failed to return"
    exit 1
elif [[ $EXIT_CODE -eq 0 ]]; then
    echo "buildbox-worker's '--validate-parameters' option failed to catch an invalid runner argument"
    exit 1
else
    echo "buildbox-worker successfully detected an invalid runner argument and exited"
fi

echo -e "\033[1;32m---[$0] succeeded.\033[0m"
