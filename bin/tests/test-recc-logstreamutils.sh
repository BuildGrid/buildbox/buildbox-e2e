#!/usr/bin/env bash
set -e

echo "[$0] Testing logstreamreceiver..."

nohup logstreamreceiver --bind=localhost:50070 1>received_data 2>/dev/null &
LSR_PID=$!
sleep 3 # Give the server time to start

# Stream stdin

LOG_MESSAGE="Hello, world!"
echo -n "$LOG_MESSAGE" | outputstreamer --remote=http://localhost:50070 --resource-name="test-stdin-log"
sleep 2

if [[ $(cat received_data) != "$LOG_MESSAGE" ]]; then
  exit 1
fi

nohup logstreamreceiver --bind=localhost:50071 1>received_data 2>/dev/null &
LSR2_PID=$!
sleep 3 # Give the server time to start

dd if=/dev/urandom of=random_blob bs=1M count=16
outputstreamer --remote=http://localhost:50071 --resource-name="test-file-log" --file=random_blob
sleep 2

diff received_data random_blob

kill $LSR_PID $LSR2_PID

echo -e "\033[1;32m---[$0] succeeded.\033[0m"
