#!/usr/bin/env bash
set -e

# Issues a simple compile command using recc in cache-only mode which fails
#
# Arguments:
#   * $1: Value to specify in RECC_REAPI_VERSION. Valid: {"2.0", "2.1", "2.2"}.

# Populating input directory:
symlinked_header_file="symlinked_test_header_file.h"
header_file="test_header_file.h"
cp "$BUILDBOX_E2E_DATA_DIR/hello.cpp" .
cp "$BUILDBOX_E2E_DATA_DIR/$header_file" .

if [[ ! -L "$symlinked_header_file" ]];
then
    ln --symbolic "$header_file" "$symlinked_header_file"
fi

# recc config:
export RECC_SERVER="invalid server"
export RECC_VERBOSE=1
export RECC_FORCE_REMOTE=1
export RECC_CACHE_UPLOAD_LOCAL_BUILD=1
export RECC_OUTPUT_FILES_OVERRIDE="hello"
export RECC_DEPS_OVERRIDE="hello.cpp,symlinked_test_header_file.h"
export RECC_INSTANCE=""
if [[ $# -eq 1 ]]; then
    export RECC_REAPI_VERSION="$1"
    echo "Set RECC_REAPI_VERSION=$RECC_REAPI_VERSION"
fi


# Create tiny gcc wrapper to allow checking whether the action was executed locally.
gcc_path="/usr/bin/gcc-0"
cat > "$gcc_path" << "EOF"
#!/bin/sh
touch gcc.executed
exec gcc "$@"
EOF
chmod a+x "$gcc_path"

rm -f hello gcc.executed

export RECC_OUTPUT_FILE=$(mktemp)

echo "Invoking recc with invalid server and no fallback"
recc "$gcc_path" -I"$(pwd)" hello.cpp -o hello 2>&1 | tee $RECC_OUTPUT_FILE

echo "recc output directory '$(pwd)': $(ls -l)"

if [ -e gcc.executed ]
then
    echo "ERROR: command executed with invalid server"
    exit 1
else
    echo "Command failed as expected"
fi

export RECC_FALLBACK_TO_LOCAL=1
echo "Invoking recc with invalid server but with fallback"
recc "$gcc_path" -I"$(pwd)" hello.cpp -o hello 2>&1 | tee $RECC_OUTPUT_FILE


if [ -e gcc.executed ]
then
    echo "Command executed in fallback mode"
else
    echo "ERROR: Command not executed even after fallback was enabled"
    exit 1
fi


if [[ "$(./hello)" = "hello_world" ]];
then
    echo "Correct recc output in fallback mode"
else
    echo "ERROR: Incorrect recc output in fallback mode"
    exit 1
fi

echo -e "\033[1;32m---[$0] succeeded.\033[0m"
