#!/usr/bin/env bash
set -e

# Issue a build command that will fail and check that the error message written
# by the compiler to standard error is streamed from the remote runner.

echo "[$0] Testing logstream with recc..."

# recc config:
export RECC_SERVER=$BUILDGRID_SERVER
export RECC_VERBOSE=1
export RECC_FORCE_REMOTE=1
export RECC_OUTPUT_FILES_OVERRIDE="compiler-error"
export RECC_DEPS_OVERRIDE="compiler-error.cpp"
export RECC_INSTANCE=""

# Input directory:
cp "$BUILDBOX_E2E_DATA_DIR/compiler-error.cpp" .

# Issuing the remote execution request:
gcc_path=$(which gcc)
recc "$gcc_path" compiler-error.cpp -o compiler-error > /tmp/recc.stdout || :

# Give some buffer for the logstream reader
sleep 5

echo "Start to compare logs"
if ! diff --side-by-side /tmp/recc.stdout "$LOGSTREAM_STDOUT_FILE";
then
    echo "Streamed standard outputs do not match"
    exit 1
elif [[ ! -s "$LOGSTREAM_STDERR_FILE" ]];
then
    echo "stderr file \"$LOGSTREAM_STDERR_FILE\" is zero bytes"
    exit 1
else
    echo "Streamed standard outputs match"
fi

echo -e "\033[1;32m---[$0] succeeded.\033[0m"
