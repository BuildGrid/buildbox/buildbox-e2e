#!/usr/bin/env bash
set -e

# Tests remote execution using trexe.

TREXE_CMD="trexe --remote=http://$BUILDGRID_SERVER"

echo hello world > hello.txt

# Test basic remote execution and stdout

$TREXE_CMD /bin/echo hello world > stdout
if [[ "$(cat stdout)" = "$(cat hello.txt)" ]];
then
    echo "Correct stdout"
else
    echo "Incorrect stdout: " $(cat stdout)
    exit 1
fi

# Test stderr

$TREXE_CMD -- /bin/bash -c "echo hello >&2" 2> stderr
if [ "$(cat stderr)" = "hello" ]; then
  echo "Correct stderr"
else
  echo "Incorrect stderr: " $(cat stderr)
  exit 1
fi

# Test executing a non-existant command

set +e
$TREXE_CMD  --result-metadata-file=result.json -- /my/fake/command someargs
if [ $? -eq 0 ]; then
    echo "Incorrect exit code"
    exit 1
fi
set -e

# Exit non-zero if error field doesn't exist
jq -e ".error" result.json

if [[ $(jq -e ".error.code" result.json) != "5" ]]; then
    echo "Incorrect error code in result.json"
    exit 1
fi
rm result.json

# Test executing a read-only command

touch read-only-command.sh
chmod 444 read-only-command.sh
command_path="$(pwd)/read-only-command.sh"
set +e
$TREXE_CMD  --result-metadata-file=result.json -- $command_path someargs
if [ $? -eq 0 ]; then
    echo "Incorrect exit code"
    exit 1
fi
set -e

# Exit non-zero if error field doesn't exist
jq -e ".error" result.json

if [[ $(jq -e ".error.code" result.json) != "3" ]]; then
    echo "Incorrect error code in result.json"
    exit 1
fi
rm result.json

# Test support of input and output files and more complex command lines

mkdir input
cp hello.txt input/

$TREXE_CMD --input-path=input --d=output --output-path=hello.b64 /bin/sh -c 'base64 <hello.txt >hello.b64'
if [[ "$(base64 <hello.txt)" = "$(cat output/hello.b64)" ]];
then
    echo "Correct output file"
else
    echo "Incorrect output file: " $(cat output/hello.b64)
    exit 1
fi

# Test support of multiple inputs

mkdir input2
echo goodbye world > goodbye.txt

cat hello.txt goodbye.txt > hellogoodbye.txt

## Ensure files are not in cwd
mv hello.txt input/
mv goodbye.txt input2/

$TREXE_CMD --input-path=./input --input-path=input2 --d=output --output-path=hellogoodbye.txt /bin/sh -c '/bin/cat hello.txt goodbye.txt > hellogoodbye.txt'
if [[ "$(cat hellogoodbye.txt)" = "$(cat output/hellogoodbye.txt)" ]];
then
    echo "Correct output file"
else
    echo "Incorrect output file: " $(cat output/hellogoodbye.txt)
    exit 1
fi

mkdir input/subdir
mkdir input2/subdir
echo foo > input/subdir/foo.txt
echo bar > input2/subdir/bar.txt

cat input/subdir/foo.txt input2/subdir/bar.txt > foobar.txt
$TREXE_CMD --input-path=input --input-path=./input2/subdir --d=output --output-path=foobar.txt /bin/sh -c '/bin/cat subdir/foo.txt bar.txt > foobar.txt'
if [[ "$(cat foobar.txt)" = "$(cat output/foobar.txt)" ]];
then
    echo "Correct output file"
else
    echo "Incorrect output file: " $(cat output/foobar.txt)
    exit 1
fi

# Test cancellation

OPERATION=$($TREXE_CMD --no-wait /bin/sleep 100)

sleep 5

$TREXE_CMD --operation=${OPERATION} --cancel

# Async cache hit

mkdir -p input/async-cache-hit out/async-cache-hit
test_date=$(date +'%s')
echo "#!/bin/bash
echo $test_date
" > input/async-cache-hit/hello.sh

chmod +x input/async-cache-hit/hello.sh

## The first build

$TREXE_CMD \
    --input-path=input/async-cache-hit --output-path=output \
    --result-metadata-file=out/async-cache-hit/result1.json "./hello.sh"

if [[ $(jq ".actionResultMetadata.cachedResult" out/async-cache-hit/result1.json) != "false" ]]; then
    exit 1
fi
# exit 1 if error
jq -e '.actionDigest | has("hash")' out/async-cache-hit/result1.json

## The second build should observe the cache hit

OUTPUT=$($TREXE_CMD --no-wait \
    --input-path=input/async-cache-hit --output-path=output \
    --result-metadata-file=out/async-cache-hit/result2.json "./hello.sh");
echo "Output: ${OUTPUT}"

if [[ $(jq ".actionResultMetadata.cachedResult" out/async-cache-hit/result2.json) != "true" ]]; then
    exit 1
fi
# exit 1 if error
jq -e '.actionDigest | has("hash")' out/async-cache-hit/result2.json

if [[ "$OUTPUT" != "$test_date" ]]; then
    diff <(echo $OUTPUT) <(echo "$test_date")
    exit 1
fi

# Async failure

mkdir -p input/async-failure out/async-failure

echo "#!/bin/bash
exit 42;
" > input/async-failure/hello.sh

chmod +x input/async-failure/hello.sh

OPERATION=$($TREXE_CMD --no-wait \
                --input-path=input/async-failure --output-path=out/async-failure/output \
                --result-metadata-file=out/async-failure/result1.json "./hello.sh")

sleep 5

set +e
$TREXE_CMD --operation="${OPERATION}" --no-wait --d=/home \
    --result-metadata-file=out/async-failure/result2.json

if [[ "$?" != 42 ]]; then
    exit 1;
fi
set -e

# Async

mkdir -p input/async out/async

echo "#!/bin/bash
echo \"Compiling a hello world\"
mkdir -p output
/usr/bin/g++ hello_world.cpp -o output/hello_world
./output/hello_world
" > input/async/hello.sh

chmod +x input/async/hello.sh

echo "#include <iostream>

int main()
{
    std::cout << \"Sent from trexe: Hello World!\" << std::endl;;
    return 0;
}" > input/async/hello_world.cpp

OPERATION=$($TREXE_CMD --no-wait \
                --input-path=input/async --output-path=out/async/output \
                --result-metadata-file=out/async/result1.json "./hello.sh")

sleep 5

$TREXE_CMD --operation="${OPERATION}" --no-wait --d=/home \
    --result-metadata-file=out/async/result2.json \
    && test -f out/async/result1.json \
    && test -f out/async/result2.json

# Test input paths remapping

mkdir -p /tmp/upload/input/a
mkdir -p /tmp/upload/input/b
mkdir -p /tmp/upload/input/c/baz

echo "foo" >/tmp/upload/input/a/foo.txt
echo "bar" >/tmp/upload/input/b/bar.txt
echo "baz" >/tmp/upload/input/c/baz/baz.txt

echo "#!/bin/bash
cat foo.txt
cat bar.txt
cat baz/baz.txt
" >/tmp/upload/input/test.sh

chmod +x /tmp/upload/input/test.sh

$TREXE_CMD --stdout-file=/tmp/stdout --skip-cache-lookup \
    --input-path=/tmp/upload/input/a/foo.txt --input-path=/tmp/upload/input/b/bar.txt \
    --input-path=/tmp/upload/input/c --input-path=/tmp/upload/input/test.sh \
    "./test.sh"

out=$(</tmp/stdout)
expected=$(printf "foo\nbar\nbaz")

if [[ "$out" != "$expected" ]]; then
    diff <(echo $out) <(echo "$expected")
    exit 1
fi

mkdir -p /tmp/upload/input/script
echo "#!/bin/bash
cat a/foo.txt
cat b/bar.txt
cat c/baz/baz.txt
" >/tmp/upload/input/script/test.sh

chmod +x /tmp/upload/input/script/test.sh

$TREXE_CMD --remote=http://controller:50051 --stdout-file=/tmp/stdout --skip-cache-lookup \
    --input-path=/tmp/upload/input/a/foo.txt:a/foo.txt --input-path=/tmp/upload/input/b/bar.txt:b/bar.txt --input-path=/tmp/upload/input/c:/c/baz/.. \
    --input-path=/tmp/upload/input/script/:./ ./test.sh

out=$(</tmp/stdout)
expected=$(printf "foo\nbar\nbaz")

if [[ "$out" != "$expected" ]]; then
    diff <(echo $out) <(echo "$expected")
    exit 1
fi

# Test TOML config

mkdir -p /tmp/toml-test/input/src

echo "foo" > /tmp/toml-test/input/src/foo.txt

echo "#!/bin/bash
cat foo.txt
cat mapped/foo.txt
echo \$BAR
" > /tmp/toml-test/input/test.sh

chmod +x /tmp/toml-test/input/test.sh

sed -e "s#@REMOTE@#http://$BUILDGRID_SERVER#" $BUILDBOX_E2E_DATA_DIR/config.trexe.toml \
    > /tmp/toml-test/input/config.trexe.toml

# Submit an async job using config file
cat /tmp/toml-test/input/config.trexe.toml
operation_id=$(trexe --config-file="/tmp/toml-test/input/config.trexe.toml")
sleep 5
# Fetch the result
trexe --config-file="/tmp/toml-test/input/config.trexe.toml" --operation "$operation_id"

out=$(</tmp/toml-test/output/stdout)
expected=$(printf "foo\nfoo\nbar")

if [[ "$out" != "$expected" ]]; then
    diff <(echo $out) <(echo "$expected")
    exit 1
fi


# Test file permission setup
mkdir -p /tmp/trexe-permission-test

echo "foo" >/tmp/trexe-permission-test/foo.txt

echo "#!/bin/bash
stat -c '%a' foo.txt
stat -c '%a' test.sh
" >/tmp/trexe-permission-test/test.sh

chmod 666 /tmp/trexe-permission-test/foo.txt
chmod +x /tmp/trexe-permission-test/test.sh

# Upload read-only input
$TREXE_CMD --verbose --stdout-file=/tmp/stdout --skip-cache-lookup --do-not-cache \
    --input-path=/tmp/trexe-permission-test/:./:ro "./test.sh"

out=$(</tmp/stdout)
expected=$(printf "444\n555")

if [[ "$out" != "$expected" ]]; then
    diff <(echo $out) <(echo "$expected")
    exit 1
fi

chmod 444 /tmp/trexe-permission-test/foo.txt
# Upload read-write input
$TREXE_CMD --verbose --stdout-file=/tmp/stdout --skip-cache-lookup --do-not-cache \
    --input-path=/tmp/trexe-permission-test:./:rw bash -c "./test.sh"

out=$(</tmp/stdout)
expected=$(printf "664\n775")

if [[ "$out" != "$expected" ]]; then
    diff <(echo $out) <(echo "$expected")
    exit 1
fi

echo -e "\033[1;32m---[$0] succeeded.\033[0m"
