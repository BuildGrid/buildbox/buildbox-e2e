#!/usr/bin/env bash
set -e

# Issues a simple compile command using recc in verification mode.
#
# Populating input directory:
symlinked_header_file="symlinked_test_header_file.h"
header_file="test_header_file.h"
cp "$BUILDBOX_E2E_DATA_DIR/hello.cpp" .
cp "$BUILDBOX_E2E_DATA_DIR/$header_file" .

if [[ ! -L "$symlinked_header_file" ]];
then
    ln --symbolic "$header_file" "$symlinked_header_file"
fi

# recc config:
export RECC_SERVER=$BUILDGRID_SERVER
export RECC_INSTANCE=""
export RECC_REAPI_VERSION="2.2"
export RECC_VERIFY=1
export RECC_LOG_LEVEL=debug
if [[ "$BUILDBOX_RUNNER" != "buildbox-run-hosttools" ]];
then
    export RECC_NO_PATH_REWRITE=1

    # A chroot image is required when running in a sandboxed environment.
    # Pull alpine image, build chroot and upload it to CAS.
    DOCK_IMG="frolvlad/alpine-gxx@sha256:a5f206ffb5b4ef2d66dd04128b62c9b3decefb4cdb1931bdfc0f4b987e9a558f"
    docker pull $DOCK_IMG
    chrootbuilder --docker-timeout 300 --extract $DOCK_IMG /alpine
    casupload --output-digest-file=chroot.digest --cas-server=http://$BUILDGRID_SERVER /alpine

    export RECC_REMOTE_PLATFORM_chrootRootDigest=$(cat chroot.digest)
fi

# Run the compiler via recc in verification mode:
gcc_path=$(which gcc)
recc "$gcc_path" -I"$(pwd)" -c hello.cpp -o hello.o |& tee recc.log

if [[ "$BUILDBOX_RUNNER" = "buildbox-run-hosttools" ]];
then
    if grep -q "File digest match" recc.log;
    then
        echo "Verification reports expected digest match"
    else
        echo "Verification failed, digests mismatched"
        exit 1
    fi
else
    if grep -q "File digest mismatch" recc.log;
    then
        # Outputs are expected to mismatch as a different compiler is used in the sandbox
        echo "Verification reports expected digest mismatch"
    else
        echo "Verification failed, unexpected digest match"
        exit 1
    fi
fi

echo -e "\033[1;32m---[$0] succeeded.\033[0m"
