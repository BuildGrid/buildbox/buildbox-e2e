#!/usr/bin/env bash
set -e # Exit if any step fails

echo -e "\e[4;32mBuildGrid/BuildBox end-to-end test suite\e[0m\n"

# This builds a Docker image that provides the environment in which the
# end-to-end tests will run and uses `docker run --rm` to execute each test script
# in its own container.

docker_socket="/var/run/docker.sock"
if [[ ! -S "$docker_socket" ]]; then
  echo "Starting dockerd..."
  dockerd &
  sleep 1
fi

# Some tools prefer connecting to the default TCP port over a socket, so
# explicitly set the option:
export DOCKER_HOST="unix:///${docker_socket}"

# Show information that confirms that Docker is up:
echo -e "\e[1;32m--- Docker version\e[0m\n"
docker version

# Preparing environment and test runner:
echo -e "\n\n\e[1;32m--- Building test environment image...\e[0m\n"
docker build --platform linux/amd64 --build-arg="BASE_IMAGE=$ENV_BASE_IMAGE" -t e2e-environment -f /buildbox-e2e/e2e-environment.dockerfile /buildbox-e2e
echo -e "\n\n\e[1;32m--- Building test runner image...\e[0m\n"
docker build --platform linux/amd64 -t e2e-runner -f /buildbox-e2e/e2e-runner.dockerfile /buildbox-e2e

echo -e "\n\n\e[1;32m--- Running end-to-end tests...\e[0m\n"

declare -a TEST_PIDS
declare -A PID_ARGS

run_test() {
  run-test.sh "$@" &
  TEST_PID=$!
  TEST_PIDS+=($TEST_PID)
  PID_ARGS[$TEST_PID]="$*"
}

run_test test-fuse-stage-empty-file.sh --privileged --device /dev/fuse -e BUILDBOX_E2E_DISABLE_WORKER=true

run_test test-worker-validate-runner-command.sh

run_test test-casdownload-casupload.sh -e BUILDBOX_E2E_DISABLE_WORKER=true

run_test test-casd-integration.sh -e BUILDBOX_E2E_DISABLE_WORKER=true
run_test test-casd-metrics.sh -e BUILDBOX_E2E_DISABLE_WORKER=true

run_test test-recc-logstreamutils.sh
run_test test-recc-logstream.sh
run_test test-recc-output.sh -e BUILDBOX_RUNNER="buildbox-run-hosttools" -e TEST_CASDOWNLOAD="true"
run_test test-recc-output.sh -e BUILDBOX_RUNNER="buildbox-run-hosttools" -e BUILDBOX_E2E_RUNNER_DISABLE_CASD=1
run_test test-recc-output.sh --privileged -v "$docker_socket":"$docker_socket" -e BUILDBOX_RUNNER="buildbox-run-userchroot"

run_test test-recc-clang-scan-deps.sh --privileged -v "$docker_socket":"$docker_socket" -e BUILDBOX_RUNNER="buildbox-run-userchroot"
run_test test-recc-cache-only.sh -e BUILDBOX_E2E_DISABLE_WORKER=1
run_test test-recc-cache-only.sh -e BUILDBOX_E2E_DISABLE_WORKER=1 -e BUILDBOX_RUNNER="buildbox-run-hosttools"

run_test test-recc-ignore-failure.sh -e BUILDBOX_E2E_DISABLE_WORKER=1
run_test test-recc-ignore-failure.sh -e BUILDBOX_E2E_DISABLE_WORKER=1 -e BUILDBOX_RUNNER="buildbox-run-hosttools"

run_test test-recc-invalid-path.sh -e BUILDBOX_E2E_DISABLE_WORKER=1
run_test test-recc-invalid-path.sh -e BUILDBOX_RUNNER="buildbox-run-hosttools"

run_test test-recc-verify.sh -e BUILDBOX_RUNNER="buildbox-run-hosttools" -e RUN_WITH_RUMBAD=true
run_test test-recc-verify.sh --privileged -v "$docker_socket":"$docker_socket" -e BUILDBOX_RUNNER="buildbox-run-userchroot" -e RUN_WITH_RUMBAD=true

run_test test-trexe.sh -e BUILDBOX_RUNNER="buildbox-run-hosttools"
run_test test-trexe-cache-only.sh -e BUILDBOX_E2E_DISABLE_WORKER=1 -e BUILDBOX_RUNNER="buildbox-run-hosttools"

run_test test-recc-output.sh --privileged -v "$docker_socket":"$docker_socket" -e BUILDBOX_RUNNER="buildbox-run-bubblewrap"
run_test test-recc-output.sh --privileged -v "$docker_socket":"$docker_socket" -e BUILDBOX_RUNNER="buildbox-run-bubblewrap" -e RUN_WITH_RUMBAD=true

run_test test-recc-clang-scan-deps.sh --privileged -v "$docker_socket":"$docker_socket" -e BUILDBOX_RUNNER="buildbox-run-bubblewrap"
run_test test-recc-verify.sh --privileged -v "$docker_socket":"$docker_socket" -e BUILDBOX_RUNNER="buildbox-run-bubblewrap" -e RUN_WITH_RUMBAD=true

run_test test-grpc-setup.sh -e DISTRIBUTED_BUILDGRID="standalone_bots.sh"

run_test test-run-oci.sh --privileged -v /var/run/docker.sock:/var/run/docker.sock -e BUILDBOX_E2E_DISABLE_WORKER=true

run_test test-recc-fallback.sh -e BUILDBOX_E2E_DISABLE_WORKER=1

FAIL=0
for pid in ${TEST_PIDS[@]}; do
  echo "Waiting for: ${PID_ARGS[$pid]}"
  if ! wait $pid; then
    FAIL=1
  fi
done

if [[ $FAIL -ne 0 ]]; then
  echo -e "\033[1;31m[$0] Some tests failed.\033[0m"
  exit 1
else
  echo -e "\033[1;32m[$0] All tests succeeded.\033[0m"
fi
