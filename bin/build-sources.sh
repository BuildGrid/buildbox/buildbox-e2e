#!/usr/bin/env bash
set -ex  # Exit if any step fails

# Builds every buildbox project for testing.
#
# Unless pointed to a source directory with $BUILDBOX_{component}_SOURCE_ROOT,
# fetches the contents of the master branch for that component.
#
# Arguments:
#   - $BUILDBOX_E2E_ROOT: the directory where everything is stored.
# Output:
#   - $BUILDBOX_E2E_ROOT/bin/ contains symlinks to the built binaries.

main()
{
    source env-vars.sh
    set_and_print_env_variables

    determine_required_builds
    compile_sources
}

determine_required_builds()
{
    echo -e "\n\n\e[1;32m--- Determining required builds...\e[0m\n"

    INSTALL_BUILDGRID=false
    if [[ -d "$BUILDGRID_SOURCE_ROOT" ]]; then
        INSTALL_BUILDGRID=true
    fi

    INSTALL_LOGSTREAM=false
    if [[ -d "$LOGSTREAM_SOURCE_ROOT" ]]; then
        INSTALL_LOGSTREAM=true
    fi

    BUILD_BUILDBOX=false
    if [[ -d "$BUILDBOX_SOURCE_ROOT" ]]; then
        BUILD_BUILDBOX=true
    fi

    BUILD_CHROOTBUILDER=false
    if [[ -d "$CHROOTBUILDER_SOURCE_ROOT" || "$BUILD_BUILDBOX" = true ]]; then
        BUILD_CHROOTBUILDER=true
    fi

    BUILD_USERCHROOT=false
    if [[ -d "$USERCHROOT_SOURCE_ROOT" || "$BUILD_BUILDBOX" = true ]]; then
        BUILD_USERCHROOT=true
    fi

    echo "INSTALL_BUILDGRID=$INSTALL_BUILDGRID"
    echo "INSTALL_LOGSTREAM=$INSTALL_LOGSTREAM"
    echo "BUILD_BUILDBOX=$BUILD_BUILDBOX"
    echo "BUILD_CHROOTBUILDER=$BUILD_CHROOTBUILDER"
    echo "BUILD_USERCHROOT=$BUILD_USERCHROOT"
}

compile_sources()
{
    echo -e "\n\n\e[1;32m--- Compiling required sources...\e[0m\n"

    echo -e "\e[0;34mSource directory ($BUILDBOX_E2E_ROOT)\e[0m"
    mkdir -p "$BUILDBOX_E2E_ROOT/bin"
    ls -hal "$BUILDBOX_E2E_ROOT"

    echo

    if [[ "$INSTALL_BUILDGRID" = true ]]; then
        echo -e "\n\e[0;34mInstalling BuildGrid\e[0m\n"

        workdir="$BUILDGRID_SOURCE_ROOT"
        if [[ ! -d "$workdir" ]]; then
            workdir="$BUILDGRID_BASE_SOURCE_ROOT"
        fi
        echo "Installing from $workdir"

        # shellcheck disable=SC1091
        (cd "$workdir" && python3 -m venv env && source env/bin/activate && pip3 install --upgrade pip && pip3 install .)
        ln -sf "$workdir/env/bin/bgd" "$BUILDBOX_E2E_ROOT/bin/bgd"
    fi

    if [[ "$INSTALL_LOGSTREAM" = true ]]; then
        echo -e "\n\e[0;34mInstalling buildgrid-logstream\e[0m\n"

        workdir="$LOGSTREAM_SOURCE_ROOT"
        if [[ ! -d "$workdir" ]]; then
            workdir="$LOGSTREAM_BASE_SOURCE_ROOT"
        fi
        echo "Installing from $workdir"

        # shellcheck disable=SC1091
        (cd "$workdir" && python3 -m venv env && source env/bin/activate && pip3 install --upgrade pip && pip3 install .)
        ln -sf "$workdir/env/bin/bgd-logstream" "$BUILDBOX_E2E_ROOT/bin/bgd-logstream"
    fi

    if [[ "$BUILD_BUILDBOX" = true ]]; then
        echo -e "\n\e[0;34mCompiling buildbox\e[0m\n"

        workdir="$BUILDBOX_SOURCE_ROOT"
        if [[ ! -d "$workdir" ]]; then
            workdir="$BUILDBOX_BASE_SOURCE_ROOT"
        fi
        echo "Compiling from $workdir"

        CMAKE_BUILDBOX_OPTS="-DBUILD_TESTING=OFF -DCMAKE_BUILD_TYPE=DEBUG -DLOGSTREAM_DEBUG=ON -DTOOLS=ON -DCLANG_SCAN_DEPS=ON"

        (cd "$workdir" && mkdir -p build && cd build && cmake -G Ninja $CMAKE_BUILDBOX_OPTS .. && ninja)
        export Buildbox_PATH=$BUILDBOX_E2E_ROOT

        ln -sf "$workdir/build/casd/buildbox-casd" "$BUILDBOX_E2E_ROOT/bin/buildbox-casd"
        ln -sf "$workdir/build/casd/test/cli_downloader" "$BUILDBOX_E2E_ROOT/bin/cli_downloader"
        ln -sf "$workdir/build/casd/test/cli_stager" "$BUILDBOX_E2E_ROOT/bin/cli_stager"
        ln -sf "$workdir/build/casd/test/cli_uploader" "$BUILDBOX_E2E_ROOT/bin/cli_uploader"
        ln -sf "$workdir/build/casdownload/casdownload" "$BUILDBOX_E2E_ROOT/bin/casdownload"
        ln -sf "$workdir/build/casupload/casupload" "$BUILDBOX_E2E_ROOT/bin/casupload"
        ln -sf "$workdir/build/fuse/buildbox-fuse" "$BUILDBOX_E2E_ROOT/bin/buildbox-fuse"
        ln -sf "$workdir/build/logstreamreceiver/logstreamreceiver" "$BUILDBOX_E2E_ROOT/bin/logstreamreceiver"
        ln -sf "$workdir/build/outputstreamer/outputstreamer" "$BUILDBOX_E2E_ROOT/bin/outputstreamer"
        ln -sf "$workdir/build/recc/recc" "$BUILDBOX_E2E_ROOT/bin/recc"
        ln -sf "$workdir/build/rexplorer/rexplorer" "$BUILDBOX_E2E_ROOT/bin/rexplorer"
        if [[ -f "$workdir/build/rumba/rumbad/rumbad" ]]; then
            ln -sf "$workdir/build/rumba/rumbad/rumbad" "$BUILDBOX_E2E_ROOT/bin/rumbad"
        else
            ln -sf "$workdir/build/rumbad/rumbad" "$BUILDBOX_E2E_ROOT/bin/rumbad"
        fi
        ln -sf "$workdir/build/run-hosttools/buildbox-run-hosttools" "$BUILDBOX_E2E_ROOT/bin/buildbox-run-hosttools"
        ln -sf "$workdir/build/run-oci/buildbox-run-oci" "$BUILDBOX_E2E_ROOT/bin/buildbox-run-oci"
        ln -sf "$workdir/build/run-userchroot/buildbox-run-userchroot" "$BUILDBOX_E2E_ROOT/bin/buildbox-run-userchroot"
        ln -sf "$workdir/build/run-bubblewrap/buildbox-run-bubblewrap" "$BUILDBOX_E2E_ROOT/bin/buildbox-run-bubblewrap"
        ln -sf "$workdir/build/trexe/trexe" "$BUILDBOX_E2E_ROOT/bin/trexe"
        ln -sf "$workdir/build/worker/buildbox-worker" "$BUILDBOX_E2E_ROOT/bin/buildbox-worker"
    fi

    if [[ "$BUILD_CHROOTBUILDER" = true ]]; then
        echo -e "\n\e[0;34mCompiling chrootbuilder\e[0m\n"

        workdir="$CHROOTBUILDER_SOURCE_ROOT"
        if [[ ! -d "$workdir" ]]; then
            workdir="$CHROOTBUILDER_BASE_SOURCE_ROOT"
        fi
        echo "Compiling from $workdir"

        (cd "$workdir" && pipx install .)
    fi

    if [[ "$BUILD_USERCHROOT" = true ]]; then
        echo -e "\n\e[0;34mCompiling userchroot\e[0m\n"

        workdir="$USERCHROOT_SOURCE_ROOT"
        if [[ ! -d "$workdir" ]]; then
            workdir="$USERCHROOT_BASE_SOURCE_ROOT"
        fi
        echo "Compiling from $workdir"

        (cd "$workdir" && mkdir -p build && cd build && cmake -G Ninja .. && ninja)
        chmod 4755 "$workdir/build/userchroot"
        ln -sf "$workdir/build/userchroot" "$BUILDBOX_E2E_ROOT/bin/userchroot"
    fi
}

main
