#!/usr/bin/env bash

set_and_print_env_variables()
{
    echo -e "\n\n\e[1;32m--- Setting environment variables...\e[0m\n"

    echo -e "\e[0;34mbuildbox\e[0m"
    echo "source root: ${BUILDBOX_SOURCE_ROOT:=$BUILDBOX_E2E_ROOT/buildbox}"
    echo "base source root: ${BUILDBOX_BASE_SOURCE_ROOT:=$BUILDBOX_E2E_BASE_ROOT/buildbox}"

    echo -e "\n\e[0;34mbuildgrid\e[0m"
    echo "source root: ${BUILDGRID_SOURCE_ROOT:=$BUILDBOX_E2E_ROOT/buildgrid}"
    echo "base source root: ${BUILDGRID_BASE_SOURCE_ROOT:=$BUILDBOX_E2E_BASE_ROOT/buildgrid}"

    echo -e "\n\e[0;34mbuildgrid-logstream\e[0m"
    echo "source root: ${LOGSTREAM_SOURCE_ROOT:=$BUILDBOX_E2E_ROOT/buildgrid-logstream}"
    echo "base source root: ${LOGSTREAM_BASE_SOURCE_ROOT:=$BUILDBOX_E2E_BASE_ROOT/buildgrid-logstream}"

    echo -e "\n\e[0;34mchrootbuilder\e[0m"
    echo "source root: ${CHROOTBUILDER_SOURCE_ROOT:=$BUILDBOX_E2E_ROOT/chrootbuilder}"
    echo "base source root: ${CHROOTBUILDER_BASE_SOURCE_ROOT:=$BUILDBOX_E2E_BASE_ROOT/chrootbuilder}"

    echo -e "\n\e[0;34muserchroot\e[0m"
    echo "source root: ${USERCHROOT_SOURCE_ROOT:=$BUILDBOX_E2E_ROOT/userchroot}"
    echo "base source root: ${USERCHROOT_BASE_SOURCE_ROOT:=$BUILDBOX_E2E_BASE_ROOT/userchroot}"
}
